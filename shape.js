var originX, originY;

var rectButton = document.getElementById('rectangle');
var circleButton = document.getElementById('circle');
var triangleButton = document.getElementById('triangle');

var previousState;

rectButton.addEventListener('click', setRect);
circleButton.addEventListener('click', setCircle);
triangleButton.addEventListener('click', setTriangle);

//requestAnimationFrame(dragRect);

function setRect() {
    state = 'rectangle';
    canvas.style.cursor = "url('image/pencil.png') 10 10, auto";
}

function setCircle() {
    state = 'circle';
    canvas.style.cursor = "url('image/pencil.png') 10 10, auto";
}

function setTriangle() {
    state = 'triangle';
    canvas.style.cursor = "url('image/pencil.png') 10 10, auto";
}

function originPoint(mousePos) {
    originX = mousePos.x;
    originY = mousePos.y;
    preImg.src = canvasArray[stepCount];
    // Store canvas state without preview.
    previousState = context.getImageData(0, 0, canvas.width, canvas.height);
}

function distance(x1, y1, x2, y2) {
    var a = x1 - x2;
    var b = y1 - y2;
    var d = Math.sqrt(a*a + b*b);
    return d; 
}

function dragRect(mousePos) {
    if (dragging) {
        // Restore canvas state without preview.
        context.clearRect(0, 0 , canvas.width, canvas.height);
        context.putImageData(previousState, 0, 0);
        // Draw preview
        drawRect(mousePos);
    }
}

function drawRect(mousePos) {
    context.beginPath();
    context.rect(originX, originY, mousePos.x - originX, mousePos.y - originY);
    context.stroke();
}

function dragCircle(mousePos) {
    if(dragging) {
        context.clearRect(0, 0 , canvas.width, canvas.height);
        context.putImageData(previousState, 0, 0);
        drawCircle(mousePos);
    }
}

function drawCircle(mousePos) {
    //context.clearRect(0, 0 , canvas.width, canvas.height);
    context.beginPath();
    var r = distance(originX, originY, mousePos.x, mousePos.y)/2;
    context.arc((originX + mousePos.x)/2, (originY + mousePos.y)/2, r, 0, 2 * Math.PI);
    context.stroke();
}

function dragTriangle(mousePos) {
    if(dragging) {
        context.clearRect(0, 0 , canvas.width, canvas.height);
        context.putImageData(previousState, 0, 0);
        drawTriangle(mousePos);
    }
}

function drawTriangle(mousePos) {
    context.beginPath();
    context.moveTo(originX, originY);
    context.lineTo(mousePos.x, mousePos.y);
    context.lineTo(2*originX - mousePos.x, mousePos.y);
    context.lineTo(originX, originY);
    context.stroke();
}