# AS_Web_Canvas
* Basic control tool
\- Brush and eraser: 
根據點擊標籤會改變cursor成brush或eraser功能，並改變icon。
\- Color selector
10 種顏色可供選擇，選擇到的顏色會以白色外框表示。顏色影響brush、triangle和text等等功能。
\-Simple menu
Radius之+和-可以調整brush寬度和text大小。text大小為10被radius(px)。
* Text input
\-在text一旁的欄位輸入欲顯示文字，點擊text後再點擊canvas可顯示文字。
\-可選擇3種字型(normal、italic、Arial)，並用Radius改變小。
* Cursor icon
\- 共有brush、eraser、圖形、text四種icon，點擊相應功能後自動改變。
* Refresh button
\- 點選Reset button後canvas變為透明，並初始化redo的紀錄。
* Different brush shape
\- 選擇rectangle button後啟用。以點擊原始點到放開滑鼠的點為對角線畫出矩形。
\- 選擇circle button後啟用。以點擊原始點到放開滑鼠的點為通過圓心的直徑畫出圓形。
\- 選擇circle button後啟用。以點擊原始點到放開滑鼠的點為等腰三角的一邊畫出等腰三角形。
* Un/Re-do button
\- 點擊undo/redo button會將canvas回復為上/下一步的狀態。滑鼠在canvas上點擊置放算一步。
* Image tool
\- 點擊選擇檔案上傳檔案後繪製在canvas中央。
* Download
\- 點選Save button 下載當前canvas圖形為png到local。
