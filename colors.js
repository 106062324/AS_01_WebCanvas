// brush state
var brushButton = document.getElementById('brush');
var eraserButton = document.getElementById('eraser');

brushButton.addEventListener('click', setBrush);
eraserButton.addEventListener('click', setEraser);

function setBrush() {
    state = 'brush';
    canvas.style.cursor = "url('image/brush.png') 10 10, auto";
}

function setEraser() {
    state = 'eraser';
    canvas.style.cursor = "url('image/eraser.png') 10 10, auto";
}

// color
var colors = ['black', 'grey', 'red', 'orange', 'yellow', 'green', 'blue', 'indigo', 'violet'];
var swatches = document.getElementsByClassName('swatch');

for(var i = 0, n = colors.length; i<n; i++) {
    var swatch = document.createElement('div');
    swatch.className = 'swatch';
    swatch.style.backgroundColor = colors[i];
    swatch.addEventListener('click', setSwatch);
    document.getElementById('colors').appendChild(swatch);
}

function setColor(color) {
    context.fillStyle = color;
    context.strokeStyle = color;
    var active = document.getElementsByClassName('active')[0];
    if(active) {
        active.className = 'swatch';
    }
}

function setSwatch(e) {
    //identify swatch
    var swatch = e.target;
    //set color
    setColor(swatch.style.backgroundColor);
    //give active class
    swatch.className += ' active';
    // set state of cursor
    console.log(state);
}

setSwatch({target: document.getElementsByClassName('swatch')[0]});