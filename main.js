var content = document.getElementById('content');
var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');
var canvasImg = new Image();
var preImg = new Image;
var state = 'brush';
var dragging = false;
var radius = 5;
var raf;

context.lineWidth = radius*2;
canvas.style.cursor = "url('image/brush.png') 10 10, auto";

function getMousePos(canvas, e) {
    var rect = canvas.getBoundingClientRect();
    return {x: e.clientX - rect.left, y: e.clientY - rect.top};
};

var cursor = {
    engage: function(e) {
        var mousePos = getMousePos(canvas, e);
        dragging = true;
        if(state != eraser) context.globalCompositeOperation="source-over";
        switch (state) {
            case 'brush':
                drawLine(mousePos);
                break;
            case 'rectangle':
                originPoint(mousePos);
                break;
            case 'circle':
                originPoint(mousePos);
                break;
            case 'triangle':
                originPoint(mousePos);
                break;
            case 'text':
                drawText(mousePos);
                break;
            case 'eraser':
                eraseDown(mousePos);
                break;
        }
    },
    move: function(e) {
        var mousePos = getMousePos(canvas, e);
        switch (state) {
            case 'brush':
                drawLine(mousePos);
                break;
            case 'rectangle':
                dragRect(mousePos);
                //raf = window.requestAnimationFrame(dragRect);
                break;
            case 'circle':
                dragCircle(mousePos);
                break;
            case 'triangle':
                dragTriangle(mousePos);
                break;
            case 'eraser':
                eraseMove(mousePos);
                break;
        }
    },
    disengage: function(e) {
        var mousePos = getMousePos(canvas, e);
        dragging = false;
        switch (state) {
            case 'brush':
                break;
            case 'rectangle':
                drawRect(mousePos);
                //raf = window.requestAnimationFrame(dragRect);
                break;
            case 'circle':
                drawCircle(mousePos);
                break;
            case 'triangle':
                drawTriangle(mousePos);
                break;
            case 'eraser':
                eraseDown(mousePos);
                break;
        }
        context.beginPath();
        PushArray();
    } 
}

var drawLine = function(mousePos) {
    if(dragging) {
        context.lineTo(mousePos.x, mousePos.y);
        context.stroke();
        context.beginPath();
        context.arc(mousePos.x, mousePos.y, radius, 0, Math.PI*2);
        context.fill();
        context.beginPath();
        context.moveTo(mousePos.x, mousePos.y);
    }
}

function eraseDown(mousePos) {
    context.globalCompositeOperation="destination-out";
    context.arc(mousePos.x, mousePos.y, radius, 0, Math.PI*2);
    context.fill();
}

function eraseMove(mousePos) {
    if(dragging) {
        context.globalCompositeOperation="destination-out";
        context.arc(mousePos.x, mousePos.y, radius, 0, Math.PI*2);
        context.fill();
    }
}

canvas.addEventListener('mousedown', cursor.engage);
canvas.addEventListener('mousemove', cursor.move);
canvas.addEventListener('mouseup', cursor.disengage);

