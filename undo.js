var canvasArray = new Array();
var stepCount = 0;

var undoButton = document.getElementById('undo');
var redoButton = document.getElementById('redo');
var resetButton = document.getElementById('reset');
canvasArray.push(document.getElementById('canvas').toDataURL());

undoButton.addEventListener('click', undoChange);
redoButton.addEventListener('click', redoChange);
resetButton.addEventListener('click', resetCanvas);

function PushArray() {
    stepCount++;
    if(stepCount < canvasArray.length) {
        canvasArray.length = stepCount;
    }
    canvasArray.push(document.getElementById('canvas').toDataURL());
}

function undoChange() {
    if(stepCount > 0) {
        stepCount--;
        //var canvasImg = new Image();
        canvasImg.src = canvasArray[stepCount];
        canvasImg.onload = function () {
            context.clearRect(0, 0, canvas.width, canvas.height);
            context.drawImage(canvasImg, 0, 0);
        }
    }
}

function redoChange() {
    if(stepCount < (canvasArray.length - 1)) {
        stepCount++;
        //var canvasImg = new Image();
        canvasImg.src = canvasArray[stepCount];
        canvasImg.onload = function () {
            context.clearRect(0, 0, canvas.width, canvas.height);
            context.drawImage(canvasImg, 0, 0);
        }
    }
}

function resetCanvas() {
    stepCount = 0;
    canvasArray.length = 0;
    context.clearRect(0, 0, canvas.width, canvas.height);
    canvasArray.push(document.getElementById('canvas').toDataURL());
}
