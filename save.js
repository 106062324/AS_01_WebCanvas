var saveButton = document.getElementById('save');
var uploadButton = document.getElementById('upload');
var fileImg;

saveButton.addEventListener('click', saveImage);
uploadButton.addEventListener('change', setUpload, false);

function saveImage() {
    var dataURL = canvas.toDataURL('image/png');
    saveButton.href = dataURL;
}

function setUpload(e) {
    var reader = new FileReader();
    reader.onload = function(event){
        fileImg = new Image();
        fileImg.src = event.target.result;
        context.drawImage(fileImg, canvas.width/2, canvas.height/2);
    }
    reader.readAsDataURL(e.target.files[0]);
    setTimeout(()=>{PushArray()}, 100);
}